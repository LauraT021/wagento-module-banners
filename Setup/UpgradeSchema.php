<?php

namespace Wagento\Banners\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
                      
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
                                
        $installer = $setup;
        $installer->startSetup();
                                
        /** Upgrade 'banners' table */
        
        if (version_compare($context->getVersion(), '1.0.9') < 0) {

            $table = $installer->getTable('Wagento_banners'); 

            $columns = [
                        'position_id'=>[
                            'type'=> \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                            'nullable' => false, 
                            'unsigned' => true, 
                            'default' => '1',
                            'comment' =>'Set the banner position, right, left, only image',
                        ],
                    ];

            $connection = $installer->getConnection();
                   
        }
        
        if (version_compare($context->getVersion(), '1.0.8') < 0) {

            $table = $installer->getTable('Wagento_banners'); 

            $columns = [
                        'btn_cta'=>[
                            'type'=> \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                            'nullable' => false, 
                            'default' => null,
                            'comment' =>'Custom bnt cta',
                        ],
                    ];

            $connection = $installer->getConnection();
                   
        }

        foreach ($columns as $name => $definition) {
            
            $connection->addColumn($table, $name, $definition);
        
        }

        $installer->endSetup();
    }
}
