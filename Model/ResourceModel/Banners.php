<?php

namespace Wagento\Banners\Model\ResourceModel;

class Banners extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
       
    protected function _construct()
    {
        $this->_init('Wagento_banners', 'banners_id');
    }
}
