Wagento Category Banner Module for UMG
===========================================

This Module for Magento® 2 Add/Update features.

Facts
-----
* version: 1.0.8

Description
-----------
March 8th, 2017
This module adds the ability to create banners that will show on the category page specified.
* Banners can be added from admin CONTENT > CATEGORY BANNERS
* To choose the category where the banners are going to show, you need to create a new file at
 ```
<theme>/Magento_Catalog/layout/catalog_category_view_id_{{category_id}}.xml
 ```
 
 
Requirements
------------
* PHP >= 5.6.5

Compatibility
-------------
* Magento >= 2.1.4

Installation Instructions
-------------------------
The Wagento Category Banner module for Magento® 2 is distributed in three formats:
* Drop-In
* [Composer Artifact](https://getcomposer.org/doc/05-repositories.md#artifact)
* [Composer VCS](https://getcomposer.org/doc/05-repositories.md#using-private-repositories)

### Install Source Files ###

The following sections describe how to install the module source files,
depending on the distribution format, to your Magento® 2 instance.

#### Drop-In ####
If you received a single ZIP file with no `composer.json` file included, extract
its contents to the project root directory. The module sources should then be
available in the following sub-directory:

    app
    └── code
        └── Wagento
            └── Banners

#### Artifact ####
If you received multiple ZIP files with `composer.json` files included, move
them to a common directory on the server. The directory
`/var/www/share/marketplace/wagento` is used in the following examples. Please
replace this path with the actual artifact directory of choice.

    /var
    └── www
        └── share
            └── marketplace
               └── wagento
                    └── banners-1.0.8.zip

Then navigate to the project root directory and run the following commands:

    composer config repositories.wagento-module-sales vcs git@git.netresearch.de:umg/blackdiamond/modules/wagento-module-banners.git
    composer require wagento/module-banners:1.0.8

#### VCS ####
If you prefer to install the module using [git](https://git-scm.com/), run the
following commands in your project root directory:

    composer config repositories.wagento-module-sales vcs git@git.netresearch.de:umg/blackdiamond/modules/wagento-module-sales.git
    composer require wagento/module-banners:1.0.8

### Enable Module ###
Once the source files are available, make them known to the application:

    ./bin/magento module:enable Wagento_Banners
    ./bin/magento setup:upgrade

Last but not least, flush cache and compile.

    ./bin/magento cache:flush
    ./bin/magento setup:di:compile

Uninstallation
--------------

The following sections describe how to uninstall the module, depending on the
distribution format, from your Magento® 2 instance.

#### Composer Git and Composer Artifact ####

To unregister the shipping module from the application, run the following command:

    ./bin/magento module:uninstall --remove-data Wagento_Sales

This will automatically remove source files and clean up the database.

#### Drop-In ####

To uninstall the module manually, run the following commands in your project
root directory:

    ./bin/magento module:disable Wagento_Banners
    rm -rf app/code/Wagento/Banners

Features
--------------


#### Admin Settings ####
Admin >> Content >> Category Banners

1) New Banner 

    * Enabled = Enabled
    * Image Position = Right | Left |  Only Image
    * Banner Image = ( preferable image between 1200*680px or more )
    * Title 
    * Button Text
    * Link
    * Target
    * Sort Order
    


Developer
---------
* Laura Trejo | [Wagento](https://www.wagento.com/) | laura@wagento.com

License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)
